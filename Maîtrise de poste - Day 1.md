# Maîtrise de poste - Day 1

## Host OS

Etant sur Windows 10, j'ai juste a aller sur le cmd pour recuperer plein d'information avec la commande `systeminfo` voici le resultat:

* Nom de la machine `LAPTOP-LM92E4KK`

* Os et version ` Microsoft Windows 10 Famille,
Version du système:                       10.0.18362 N/A version 18362`

* Architecture du processeur `x64-based PC`

* Model du processeur `Intel® Core™ i7-8565U 1.8 GHz `

* Taille et model de la ram `Mémoire physique totale:                    16 198 Mo
Mémoire physique disponible:                9 023 Mo, model : MT52L1G32D4PG-093`

## Devices

* information processeur `Intel(R) Core(TM) i7-8565U CPU @ 1.80GHz, coeur physique 4, coeur logique 8`

* signification nomenclature i7-8565U le i7 signifie la gamme, le premier chiffre donne la generation du processeur creer (la 8eme donc) 565 c'est le numero de serie et le U signifie basse consomation utilisé pour les ultrabook comme le mien ;) 

* Touchpad:

* Enceinte avec la commande `Get-CimInstance win32_sounddevice`:

```
Manufacturer         Name                                                Status StatusInfo
------------         ----                                                ------ ----------
Realtek              Realtek High Definition Audio                       OK              3
Intel(R) Corporation Son Intel(R) pour écrans                            OK              3
NVIDIA               NVIDIA Virtual Audio Device (Wave Extensible) (WDM) OK              3
```

* Disque dur avec la commande `wmic diskdrive get model`:
```
Model
SAMSUNG MZVLB1T0HALR-00000
```

Pour avoir les informations du disque dur ou de son ssd concernant les partitions ou autre il faut sur notre cmd taper diskpart et accepter la requete que windows nous envoie, ce qui va nous renvoyer vers un second cmd appeler diskpart dans cette fenetre la on tape la commande `list disk` ou nous auront donc la liste de tous les disque dur, dans on cas j'en ai qu'un donc je fais un `select disk 0` pour dire que je selectionne donc mon disque principale. ensuite je fais un `detail disk` ce qui me renvoie : 

```
  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
  Volume 0     C   OS           NTFS   Partition    952 G   Sain       Démarrag
  Volume 1         SYSTEM       FAT32  Partition    260 M   Sain       Système
```

* Le volume 0 c'est mon os donc windows 10 avec toutes mes données a l'interieur que sa soit perso ou system.
* Le volume 1 est une partition system qui permet le bootage de l'os par exemple les information system, la corbeille etc

## Network

* Avec la commande `Get-NetAdapter` la commande retourne les informations suivantes.

```
Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
VirtualBox Host-Only ...3 VirtualBox Host-Only Ethernet Adap...#3      26 Up           0A-00-27-00-00-1A         1 Gbps
VirtualBox Host-Only ...2 VirtualBox Host-Only Ethernet Adap...#2      25 Up           0A-00-27-00-00-19         1 Gbps
VirtualBox Host-Only ...1 VirtualBox Host-Only Ethernet Adapter        22 Up           0A-00-27-00-00-16         1 Gbps
VMware Network Adapte...8 VMware Virtual Ethernet Adapter for ...       8 Up           00-50-56-C0-00-08       100 Mbps
VMware Network Adapte...1 VMware Virtual Ethernet Adapter for ...       7 Up           00-50-56-C0-00-01       100 Mbps
Wi-Fi                     Intel(R) Wireless-AC 9560 160MHz              3 Up           D0-AB-D5-7B-B4-FC       195 Mbps
```

* La carte `VirtualBox Host-Only ...3` est une carte réseau que j'ai créer a partir de virtualbox qui me permet d'avoir un lien mon hote et ma vm.
* La carte `VirtualBox Host-Only ...2` est la même chose que celle d'au dessus.
* La carte `VirtualBox Host-Only ...1` est la même chose que celle d'au dessus.
* La carte `VMware Network Adapte...8` est la même chose qu'une carte VirtualBox mais elle a été créer avec le logiciel VMware.
* La carte `VMware Network Adapte...1` est la même chose que celle d'au dessus.
* La carte `Wi-Fi` est ma carte qui me permet à me connecter à un réseau WIFI.

* En utilisant la commande `netstat` nous pouvons voir les ports qui communique avec mon pc actuellement, voici la liste : 

```
  TCP    192.168.0.13:58409     162.159.135.234:https  ESTABLISHED
  TCP    192.168.0.13:58616     52.114.88.31:https     ESTABLISHED
  TCP    192.168.0.13:58624     52.114.75.18:https     ESTABLISHED
```

* Des port tcp communique avec mon pc actuellement l'adresse d'emission c'est la mienne `192.168.0.13` c'est mon ip local, et l'addresse avec laquelle je communique est par exemple `52.114.88.31` c'est l'adresse ip dans laquelle est hébergé une page web sur laquelle je consulte actuellement. Et les autres adresses sont aussi des pages web nous pouvons le savoir par rapport au protocole utilisé le https qui est le numero de port 443.

## User

* En utilisant la commande `net user` nous avons la liste de tous les users du pc, voici ma liste de user : 

```
comptes d’utilisateurs de \\LAPTOP-LM92E4KK

-------------------------------------------------------------------------------
33623                    Administrateur           DefaultAccount
Invité                   WDAGUtilityAccount
```

* Le user 33623 est l'administrateur de ma machine.
* Ensuite nous avons le compte Invité comme dans tous les windows même si le compte invité est désactiver sur mon pc il apparait quand même.

## Processus

* En faisant un `tasklist` nous avons une grande liste de processus qui apparait alors voici 5 process windows essentiels parmis la grande liste : 

* `wininit.exe` est l'application de demarage windows.
* `rundll32.exe` est un process qui permet au system de pouvoir lire un fichier dll.
* `userinit.exe` est un process qui permet de se connecter a sa session.
* `winlogon.exe` est un process lié a userinit.exe il permet de chargé le profil du user choisis apres son authentification
* `sihost.exe ` est un process qui gere certains aspects graphiques

## Scripting

Tous les scripts sont sur le repo, tu peux les tester sur ta vm ^^

## Gestion de soft

Un gestionnaire de paquet est super utile sur notre environnement car il permet d'avoir plus confiance sur ce que l'ont télécharge.
Car les paquets installé ont déja était approuvé par notre éditeur ( Microsoft pour moi )cela évité donc tous ce qui est logiciel malveillant comme cheval de troie, ransomware etc...

## Chiffrement et notion de confiance 

* Désolé pour le screen mais je pense que ce schema expliquera mieux que moi 

![](https://i.imgur.com/Ptm2Iki.jpg)
