﻿# Script pour eteindre le pc ou le verouiller

# version 1.0
# Auteur : Marc Texier

$valeur = Read-Host "Temps d'attente avant l'éxécution du script"
$eteindre = Read-Host "Pour éteindre taper oui"
Start-Sleep -Seconds $valeur
if ($eteindre -eq "oui") {
    Stop-Computer
} else {
    rundll32.exe user32.dll,LockWorkStation
}